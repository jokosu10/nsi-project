<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;

class Poi extends Model
{

	use PostgisTrait;


	protected $postgisFields = [
		'geom',
	];

	protected $postgisTypes = [
		'geom' => [
			'geomtype' => 'geometry',
			'srid' => 4326
		],
	];

}
