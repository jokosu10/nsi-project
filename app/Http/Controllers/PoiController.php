<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Poi;
use DB;

class PoiController extends Controller
{

    // show all list
    public function index()
    {
        $data = DB::table('pois')->get();
        return response()->json(['status' => 'List data', 'data' => $data]);
    }

    // show by id
    public function show($id)
    {
        $data = DB::table('pois')->select('name', 'geom')->where('id', $id)->get();
        return response()->json(['status' => 'List data', 'data' => $data]);
    }

    // store new data
    public function store(Request $request)
    {
        $poi = new Poi();
        $poi->name = $request->name;
        $poi->geom = new Point($request->lat, $request->lng);
        $poi->save();

        return response()->json(['status' => 'Data Created', 'data' => $poi]);
    }

     // update some data
    public function edit(Request $request, $id)
    {
        $namePoi = $request->name;
        $latPoi = $request->lat;
        $lngPoi = $request->lng;

        DB::table('pois')
            ->where('id', $id)
            ->update(['name' =>  $namePoi, 'geom' => new Point($latPoi, $lngPoi)]);

        return response()->json(['status' => 'Data Updated', 'data' => $poi]);
    }

     // delete data
    public function destroy($id)
    {
        DB::table('pois')
            ->where('id', $id)
            ->delete();

        return response()->json(['status' => 'Data Deleted']);
    }
}
