<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Area;
use DB;

class AreaController extends Controller
{

    // show all list
    public function index()
    {
        $data = DB::table('areas')->get();
        return response()->json(['status' => 'List data', 'data' => $data]);
    }

    // show by id
    public function show($id)
    {
        $data = DB::table('areas')->select('name', 'geom')->where('id', $id)->get();
        return response()->json(['status' => 'List data', 'data' => $data]);
    }

    // store new data
    public function store(Request $request)
    {

        $linestring = new LineString(
            [
                new Point($request->pos1x, $request->pos1y),
                new Point($request->pos2x, $request->pos2y),
                new Point($request->pos3x, $request->pos3y),
                new Point($request->pos4x, $request->pos4y),
                new Point($request->pos5x, $request->pos5y)
            ]
        );


        $area = new Area();
        $area->name = $request->name;
        $area->geom = new Polygon([$linestring]);
        $area->luas = $request->luas;
        $area->save();

        return response()->json(['status' => 'Data Created', 'data' => $area]);
    }

     // update some data
    public function edit(Request $request, $id)
    {
        $nameArea = $request->name;
        $geomArea = $request->geom;
        $luasArea = $request->luas;

        $area = Area::find($id);
        $area->name = $nameArea;
        $area->geom = $geomArea;
        $area->luas = $luasArea;
        $area->save();

        return response()->json(['status' => 'Data Updated', 'data' => $area]);
    }

     // delete data
    public function destroy($id)
    {
        DB::table('areas')
            ->where('id', $id)
            ->delete();

        return response()->json(['status' => 'Data Deleted']);
    }
}
