<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Poi;
use App\Area;
use DB;

class MapController extends Controller
{
    public function index()
    {
        return view('map');
    }

    public function getData()
    {
        $dataPoi = DB::table('pois')->select('name', 'ST_AsGeoJSON(geom,4326) as point')->get();
        $dataArea = DB::table('area')->select('name', 'ST_AsGeoJSON(geom,4326) as polygon', 'luas')->get();
        return response()->json(['point' => $dataPoi, 'polygon' => $dataArea]);
    }
}
