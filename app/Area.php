<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;

class Area extends Model
{
    use PostgisTrait;

    //protected $fillable = ["name", "geom", "luas"];

    protected $postgisFields = [
		'geom',
	];

	protected $postgisTypes = [
		'geom' => [
			'geomtype' => 'geometry',
			'srid' => 4326
		],
	];

}
