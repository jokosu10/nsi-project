<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/area', 'AreaController@index');
Route::get('/area/{id}', 'AreaController@show');
Route::post('/area', 'AreaController@store');
Route::put('/area/{id}', 'AreaController@edit');
Route::delete('/area/{id}', 'AreaController@destroy');

Route::get('/poi', 'PoiController@index');
Route::get('/poi/{id}', 'PoiController@show');
Route::post('/poi', 'PoiController@store');
Route::put('/poi/{id}', 'PoiController@edit');
Route::delete('/poi/{id}', 'PoiController@destroy');
