<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    {{-- <link rel="stylesheet" href="{{ asset('assets/jquery/jquery-ui.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('assets/leaflet/Control.OSMGeocoder.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('assets/leaflet/leaflet.css') }}"> --}}
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin=""></script>
    <style>
        #mapid { height: 180px; }
    </style>
</head>
<body>
    <h4>Maps</h4>
    <div id="mapid"></div>
</body>
{{-- <script src="{{ asset('assets/jquery/jquery.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('assets/jquery/jquery-ui.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('assets/leaflet/leaflet.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('assets/leaflet/Control.OSMGeocoder.js') }}" type="text/javascript"></script> --}}
<script>

	var mymap = L.map('mapid').setView([51.505, -0.09], 13);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap);

	L.marker([51.5, -0.09]).addTo(mymap);

	L.circle([51.508, -0.11], {
		color: 'red',
		fillColor: '#f03',
		fillOpacity: 0.5,
		radius: 500
	}).addTo(mymap);

	L.polygon([
		[51.509, -0.08],
		[51.503, -0.06],
		[51.51, -0.047]
	]).addTo(mymap);


</script>
<script>
    var fields = ["nama_prop", "klaim_bruto", "kontribusi_bruto"];
    $(document).ready(
        getData()
    );

    // Layer OSM
    var osmAttrib = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    osmUrl = 'https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw';

    var osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});
    var osmGeocoder = new L.Control.OSMGeocoder();

    var map = L.map('map', {
        center: [-0.21973, 118.91602],
        zoom: 5,
        layers: [osm]
    });

    map.addControl(osmGeocoder);

    // Get Data From Databases
    function getData(){
        $.ajax("{{ route('data.map') }}", {
            success : function(data){
            mapData(data);
            }
        });

    function mapData(data){
        var geojson = {
            "type" : "featureCollection",
            "features" : []
        };

        var dataArray = data.split(", ;");
        dataArray.pop();

    // Add Data To GeoJson
    dataArray.forEach(function(d){
        d = d.split(", ");

        var feature = {
            "type": "Feature",
            "properties": {},
            "geometry": JSON.parse(d[fields.length])
        };

        for (var i=0; i<fields.length; i++){
            feature.properties[fields[i]] = d[i];
        };
        geojson.features.push(feature);
    });

    console.log(geojson);
    };
</script>
</html>
