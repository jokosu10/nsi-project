### Cara menjalankan program
=============================

**Spesifikasi minimal software untuk menjalankan program**
* php minimal versi **7.2**.
* terminal / command prompt.
* postgresql minimal versi **11.0**.
* pgadmin minimal versi **4.0**.
* postman.
* git
* postgis ekstension
* browser (google chrome).

=============================

**Cara setting program**
1. Buka terminal / command prompt.
2. ketikkan perintah `git clone https://gitlab.com/jokosu10/nsi-project.git`
3. Masuk ke directory aplikasi.
4. Jalankan web server php, ketik `php artisan serve` pastikan terdapat tulisan laravel.
5. Buka pgadmin aktifkan ekstensi `hstore`, `plpgsql`, `postgis`, `postgis_topology`
6. import file database `postgis-nsi`
7. copy file `.env.example` menjadi file `.env`
8. setting konfigurasi aplikasi dan database di file `.env`
9. ketikan perintah `composer install` pada terminal untuk menginstall depedency


**Cara akses API di postman**

1. Buka postman anda.
2. import collection postman dari link berikut `https://www.getpostman.com/collections/ec3982b4061eaedd38fd`
3. klik salah satu method.
4. silahkan masukkan sesuai path dan method yang ada.
5. disetiap method terdapat contoh response api yang berhasil.


**Cara menjalankan program di browser**

1. Buka browser ketikan alamat `http://localhost:8000/map`.


